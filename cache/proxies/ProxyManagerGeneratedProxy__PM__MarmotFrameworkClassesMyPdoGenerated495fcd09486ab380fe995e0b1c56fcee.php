<?php

namespace ProxyManagerGeneratedProxy\__PM__\Marmot\Framework\Classes\MyPdo;

class Generated495fcd09486ab380fe995e0b1c56fcee extends \Marmot\Framework\Classes\MyPdo implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5d1dad367cde0286637764 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5d1dad367db79259573520 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5d1dad3677b4b400201104 = array(
        'statement' => true,
        'options' => true,
    );

    private static $signature495fcd09486ab380fe995e0b1c56fcee = 'YTozOntzOjk6ImNsYXNzTmFtZSI7czozMDoiTWFybW90XEZyYW1ld29ya1xDbGFzc2VzXE15UGRvIjtzOjc6ImZhY3RvcnkiO3M6NTA6IlByb3h5TWFuYWdlclxGYWN0b3J5XExhenlMb2FkaW5nVmFsdWVIb2xkZXJGYWN0b3J5IjtzOjE5OiJwcm94eU1hbmFnZXJWZXJzaW9uIjtzOjU6IjEuMC4wIjt9';

    /**
     * {@inheritDoc}
     */
    public function setAttr($param, $val = '')
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'setAttr', array('param' => $param, 'val' => $val), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->setAttr($param, $val);
    }

    /**
     * {@inheritDoc}
     */
    public function prepare($sql = '')
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'prepare', array('sql' => $sql), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->prepare($sql);
    }

    /**
     * {@inheritDoc}
     */
    public function exec($sql)
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'exec', array('sql' => $sql), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->exec($sql);
    }

    /**
     * {@inheritDoc}
     */
    public function query($sql)
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'query', array('sql' => $sql), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->query($sql);
    }

    /**
     * {@inheritDoc}
     */
    public function beginTA()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'beginTA', array(), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->beginTA();
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'commit', array(), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollBack()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'rollBack', array(), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->rollBack();
    }

    /**
     * {@inheritDoc}
     */
    public function lastInsertId()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'lastInsertId', array(), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->lastInsertId();
    }

    /**
     * {@inheritDoc}
     */
    public function execute($param = '')
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'execute', array('param' => $param), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->execute($param);
    }

    /**
     * {@inheritDoc}
     */
    public function bindParam($parameter, $variable, $dataType = 2, $length = 6)
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'bindParam', array('parameter' => $parameter, 'variable' => $variable, 'dataType' => $dataType, 'length' => $length), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->bindParam($parameter, $variable, $dataType, $length);
    }

    /**
     * {@inheritDoc}
     */
    public function rowCount()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'rowCount', array(), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->rowCount();
    }

    /**
     * {@inheritDoc}
     */
    public function count()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'count', array(), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->count();
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'close', array(), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->close();
    }

    /**
     * {@inheritDoc}
     */
    public function closeCursor()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'closeCursor', array(), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->closeCursor();
    }

    /**
     * {@inheritDoc}
     */
    public function insert($table, array $data)
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'insert', array('table' => $table, 'data' => $data), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->insert($table, $data);
    }

    /**
     * {@inheritDoc}
     */
    public function update($table, array $data, $wheresqlArr = '')
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'update', array('table' => $table, 'data' => $data, 'wheresqlArr' => $wheresqlArr), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->update($table, $data, $wheresqlArr);
    }

    /**
     * {@inheritDoc}
     */
    public function delete($table, $wheresqlArr = '')
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'delete', array('table' => $table, 'wheresqlArr' => $wheresqlArr), $this->initializer5d1dad367db79259573520);

        return $this->valueHolder5d1dad367cde0286637764->delete($table, $wheresqlArr);
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        unset($this->statement, $this->options);

        $this->initializer5d1dad367db79259573520 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, '__get', array('name' => $name), $this->initializer5d1dad367db79259573520);

        if (isset(self::$publicProperties5d1dad3677b4b400201104[$name])) {
            return $this->valueHolder5d1dad367cde0286637764->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5d1dad367cde0286637764;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5d1dad367cde0286637764;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5d1dad367db79259573520);

        if (isset(self::$publicProperties5d1dad3677b4b400201104[$name])) {
            return ($this->valueHolder5d1dad367cde0286637764->$name = $value);
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5d1dad367cde0286637764;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5d1dad367cde0286637764;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, '__isset', array('name' => $name), $this->initializer5d1dad367db79259573520);

        if (isset(self::$publicProperties5d1dad3677b4b400201104[$name])) {
            return isset($this->valueHolder5d1dad367cde0286637764->$name);
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5d1dad367cde0286637764;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5d1dad367cde0286637764;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, '__unset', array('name' => $name), $this->initializer5d1dad367db79259573520);

        if (isset(self::$publicProperties5d1dad3677b4b400201104[$name])) {
            unset($this->valueHolder5d1dad367cde0286637764->$name);

            return;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5d1dad367cde0286637764;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5d1dad367cde0286637764;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, '__clone', array(), $this->initializer5d1dad367db79259573520);

        $this->valueHolder5d1dad367cde0286637764 = clone $this->valueHolder5d1dad367cde0286637764;
    }

    public function __sleep()
    {
        $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, '__sleep', array(), $this->initializer5d1dad367db79259573520);

        return array('valueHolder5d1dad367cde0286637764');
    }

    public function __wakeup()
    {
        unset($this->statement, $this->options);
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5d1dad367db79259573520 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5d1dad367db79259573520;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5d1dad367db79259573520 && $this->initializer5d1dad367db79259573520->__invoke($this->valueHolder5d1dad367cde0286637764, $this, 'initializeProxy', array(), $this->initializer5d1dad367db79259573520);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5d1dad367cde0286637764;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5d1dad367cde0286637764;
    }


}
